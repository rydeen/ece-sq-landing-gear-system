package system.exceptions;

/**
 * NullStringException exception.
 */
public class NullStringException extends Exception {

	private static final long serialVersionUID = 1L;
	
	// field that generated the exception
	private String wrongfield ;
	
	
	/**
	 * Class constructor.
	 * @param f
	 * @param v
	 */
	public NullStringException( String f ) {
		this.wrongfield = f ;
	}
	
	/**
	 * Prints the error occurred.
	 */
	public void printError() {
		System.out.println("\n***********");
		System.out.println("[EXCEPTION] \t NullStringEception. " + this.wrongfield + " can not be null.");
		System.out.println("***********\n");
	}
	
}
