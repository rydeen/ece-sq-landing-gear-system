package system.exceptions;

/**
 * InvalidGearSensorType exception.
 */
public class InvalidGearSensorType extends Exception {

	private static final long serialVersionUID = 1L;

	private String received ;
	private String expected ;
	private String who ;
	
	/**
	 * Constructor class.
	 * @param value
	 */
	public InvalidGearSensorType( String receivedValue, String expectedValue, String objectid ) {
		this.received = receivedValue ;
		this.expected = expectedValue ;
		this.who = objectid ;
	}
	
	/**
	 * Prints the error occurred.
	 */
	public void printError() 
	{
		System.out.println("\n***********");
		System.out.println("[EXCEPTION] \t InvalidGearSensorType. Received type=" + this.received + 
				". Expected value was '" + this.expected + "'. Object with id=" + this.who + " may not function as expected." );
		System.out.println("***********\n");
	}
}
