package system.exceptions;

public class InvalidActionOrder extends Exception {

	private static final long serialVersionUID = 1L;

	public void printError() 
	{
		System.out.println("\n***********");
		System.out.println("[EXCEPTION] \t InvalidActionOrder. The order must be 1 or 0." );
		System.out.println("***********\n");
	}
}
