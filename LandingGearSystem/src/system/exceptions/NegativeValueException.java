package system.exceptions;

public class NegativeValueException extends Exception {

	private static final long serialVersionUID = 1L;
	
	// field that generated the exception
	private String wrongfield ;
	// value of that field
	private float wrongvalue ;
	
	
	/**
	 * Class constructor.
	 * @param f
	 * @param v
	 */
	public NegativeValueException( String f , float v ) {
		this.wrongfield = f ;
		this.wrongvalue = v ;
	}
	
	
	/**
	 * Prints the error occurred.
	 */
	public void printError() {
		System.out.println("\n***********");
		System.out.println("[EXCEPTION] \t NegativeValueException. " + this.wrongfield+"="+ this.wrongvalue + " is not valid.");
		System.out.println("***********\n");
	}
}
