package system.exceptions;

/**
 * InvalidSystemStateException exception.
 */
public class InvalidSystemStateException extends Exception {

	private static final long serialVersionUID = 1L;

	private int stateReceived ;
	
	/**
	 * Constructor class.
	 * @param value
	 */
	public InvalidSystemStateException( int value ) {
		this.stateReceived = value ;
	}
	
	/**
	 * Prints the error occurred.
	 */
	public void printError() 
	{
		System.out.println("\n***********");
		System.out.println("[EXCEPTION] \t InvalidSystemStateException. State=" + this.stateReceived + " is not a valid state.");
		System.out.println("***********\n");
	}
	
	
}
