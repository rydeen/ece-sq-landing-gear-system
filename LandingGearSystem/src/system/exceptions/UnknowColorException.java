package system.exceptions;

/**
 * UnknowColorException exception
 *
 */
public class UnknowColorException extends Exception {

	private static final long serialVersionUID = 1L;

	// value that caused the exceptions
	private String colorReceived ;
	
	/**
	 * Class constructor.
	 * @param c
	 */
	public UnknowColorException( String c ) {
		this.colorReceived = c ;
	}
	
	/**
	 * Prints the error occurred.
	 */
	public void printError() {
		System.out.println("\n***********");
		System.out.println("[EXCEPTION] \t UnknowColorException. Color=" + this.colorReceived + " is not a valid Light color.");
		System.out.println("***********\n");
	}
}
