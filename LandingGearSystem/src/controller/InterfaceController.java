package controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.shape.Circle;

public class InterfaceController {

	// System State
	@FXML Circle redLight;
	@FXML Circle orangeLight;
	@FXML Circle greenLight;

	// Door lights
	@FXML Circle d1r ; @FXML Circle d1o ; @FXML Circle d1g ;
	@FXML Circle d2r ; @FXML Circle d2o ; @FXML Circle d2g ;
	@FXML Circle d3r ; @FXML Circle d3o ; @FXML Circle d3g ;
	
	// Gear lights
	@FXML Circle g1r ; @FXML Circle g1o ; @FXML Circle g1g ;
	@FXML Circle g2r ; @FXML Circle g2o ; @FXML Circle g2g ;
	@FXML Circle g3r ; @FXML Circle g3o ; @FXML Circle g3g ;
	
	
	/**
	 * Shutdown the application.
	 * @param event
	 */
	@FXML
	public void exitApp( ActionEvent event )
	{	
		Platform.exit();
		System.exit(0);
	}
	
	
	
	/**
	 * Handle UP button
	 * @param event
	 */
	@FXML
	public void handleUP( ActionEvent event )
	{	
		if ( Main.app.lightPanel.getSystemState() == 1 ) // system state = extended
		{
			Main.app.handle.changeState() ;
		}
	}
	
	/**
	 * Handle DOWN button
	 * @param event
	 */
	@FXML
	public void handleDOWN( ActionEvent event )
	{
		if ( Main.app.lightPanel.getSystemState() == 0 ) // system state = retracted
		{
			Main.app.handle.changeState() ;
		}
	}
	
	
	
	/**
	 * Change lights on gui
	 * @param value
	 */
	public void lights( int value ) {
		if ( value == 0 ) {
			turnOffOrange() ;
			turnOffRed() ;
			turnOffGreen() ;
		}
		else if ( value==1 ) {
			turnOnGreen() ;
			turnOffOrange() ;
			turnOffRed() ;
		}
		else if ( value==2 ) {
			turnOnOrange() ;
			turnOffRed() ;
			turnOffGreen() ;
		}
		else if ( value==3 ) {
			turnOnRed() ;
			turnOffOrange() ;
			turnOffGreen() ;
		}
	}
	
	/**
	 * Turns on green light
	 * @param event
	 */
	protected void turnOnGreen()
	{
		greenLight.setStyle("-fx-fill: #008000; -fx-stroke-width:1");
	}
	/**
	 * Turns on green light
	 * @param event
	 */
	protected void turnOffGreen()
	{
		greenLight.setStyle("-fx-fill: #F4F4F4; -fx-stroke-width:1");
	}
	/**
	 * Turns on red light
	 * @param event
	 */
	protected void turnOnRed()
	{
		redLight.setStyle("-fx-fill: #FF0000; -fx-stroke-width:1");
	}
	/**
	 * Turns on red light
	 * @param event
	 */
	protected void turnOffRed()
	{
		redLight.setStyle("-fx-fill: #F4F4F4; -fx-stroke-width:1");
	}
	/**
	 * Turns on orange light
	 * @param event
	 */
	protected void turnOnOrange()
	{
		orangeLight.setStyle("-fx-fill: #FFA500; -fx-stroke-width:1");
	}
	/**
	 * Turns on orange light
	 * @param event
	 */
	protected void turnOffOrange()
	{
		orangeLight.setStyle("-fx-fill: #F4F4F4; -fx-stroke-width:1");
	}

	
	
	/**
	 * Updates the state of doors on GUI
	 * @param gearsCurrentState
	 */
	public void doorLights(int[] gearsCurrentState) {
		/*
		 * States available:
		 * 		1. Open - Extend
		 * 		2. Close - Retract
		 * 		3. Maneuvering
		 */
		if ( gearsCurrentState[0] == 1 ) {
			d1g.setStyle("-fx-fill: #008000;"); // on
			d1o.setStyle("-fx-fill: #F4F4F4;") ; // off
			d1r.setStyle("-fx-fill: #F4F4F4;") ; // off
		} else if ( gearsCurrentState[0] == 2 ) {
			d1g.setStyle("-fx-fill: #F4F4F4;"); // off
			d1o.setStyle("-fx-fill: #F4F4F4;") ; // off
			d1r.setStyle("-fx-fill: #F4F4F4;") ; // off
		} else {
			d1g.setStyle("-fx-fill: #F4F4F4;"); // off
			d1o.setStyle("-fx-fill: #FFA500;") ; // on
			d1r.setStyle("-fx-fill: #F4F4F4;") ; // off
		}
		
		if ( gearsCurrentState[1] == 1 ) {
			d2g.setStyle("-fx-fill: #008000;"); // on
			d2o.setStyle("-fx-fill: #F4F4F4;") ; // off
			d2r.setStyle("-fx-fill: #F4F4F4;") ; // off
		} else if ( gearsCurrentState[1] == 2 ) {
			d2g.setStyle("-fx-fill: #F4F4F4;"); // off
			d2o.setStyle("-fx-fill: #F4F4F4;") ; // off
			d2r.setStyle("-fx-fill: #F4F4F4;") ; // off
		} else {
			d2g.setStyle("-fx-fill: #F4F4F4;"); // off
			d2o.setStyle("-fx-fill: #FFA500;") ; // on
			d2r.setStyle("-fx-fill: #F4F4F4;") ; // off
		}
		
		if ( gearsCurrentState[2] == 1 ) {
			d3g.setStyle("-fx-fill: #008000;"); // on
			d3o.setStyle("-fx-fill: #F4F4F4;") ; // off
			d3r.setStyle("-fx-fill: #F4F4F4;") ; // off
		} else if ( gearsCurrentState[2] == 2 ) {
			d3g.setStyle("-fx-fill: #F4F4F4;"); // off
			d3o.setStyle("-fx-fill: #F4F4F4;") ; // off
			d3r.setStyle("-fx-fill: #F4F4F4;") ; // off
		} else {
			d3g.setStyle("-fx-fill: #F4F4F4;"); // off
			d3o.setStyle("-fx-fill: #FFA500;") ; // on
			d3r.setStyle("-fx-fill: #F4F4F4;") ; // off
		}
	}

	/**
	 * Updates the state of gears on GUI
	 * @param doorsCurrentState
	 */
	public void gearLights(int[] doorsCurrentState) {
		/*
		 * States available:
		 * 		1. Open - Extend
		 * 		2. Close - Retract
		 * 		3. Maneuvering
		 */
		if ( doorsCurrentState[0] == 1 ) {
			g1g.setStyle("-fx-fill: #008000;"); // on
			g1o.setStyle("-fx-fill: #F4F4F4;") ; // off
			g1r.setStyle("-fx-fill: #F4F4F4;") ; // off
		} else if ( doorsCurrentState[0] == 2 ) {
			g1g.setStyle("-fx-fill: #F4F4F4;"); // off
			g1o.setStyle("-fx-fill: #F4F4F4;") ; // off
			g1r.setStyle("-fx-fill: #F4F4F4;") ; // off
		} else {
			g1g.setStyle("-fx-fill: #F4F4F4;"); // off
			g1o.setStyle("-fx-fill: #FFA500;") ; // on
			g1r.setStyle("-fx-fill: #F4F4F4;") ; // off
		}
		
		if ( doorsCurrentState[1] == 1 ) {
			g2g.setStyle("-fx-fill: #008000;"); // on
			g2o.setStyle("-fx-fill: #F4F4F4;") ; // off
			g2r.setStyle("-fx-fill: #F4F4F4;") ; // off
		} else if ( doorsCurrentState[1] == 2 ) {
			g2g.setStyle("-fx-fill: #F4F4F4;"); // off
			g2o.setStyle("-fx-fill: #F4F4F4;") ; // off
			g2r.setStyle("-fx-fill: #F4F4F4;") ; // off
		} else {
			g2g.setStyle("-fx-fill: #F4F4F4;"); // off
			g2o.setStyle("-fx-fill: #FFA500;") ; // on
			g2r.setStyle("-fx-fill: #F4F4F4;") ; // off
		}
		
		if ( doorsCurrentState[2] == 1 ) {
			g3g.setStyle("-fx-fill: #008000;"); // on
			g3o.setStyle("-fx-fill: #F4F4F4;") ; // off
			g3r.setStyle("-fx-fill: #F4F4F4;") ; // off
		} else if ( doorsCurrentState[2] == 2 ) {
			g3g.setStyle("-fx-fill: #F4F4F4;"); // off
			g3o.setStyle("-fx-fill: #F4F4F4;") ; // off
			g3r.setStyle("-fx-fill: #F4F4F4;") ; // off
		} else {
			g3g.setStyle("-fx-fill: #F4F4F4;"); // off
			g3o.setStyle("-fx-fill: #FFA500;") ; // on
			g3r.setStyle("-fx-fill: #F4F4F4;") ; // off
		}
	}
	
}
