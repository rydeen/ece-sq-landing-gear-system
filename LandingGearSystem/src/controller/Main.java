package controller;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.App;


public class Main extends Application {
	
	static public InterfaceController guiController ;
	static public Parent root ;
	static public App app ;
	
	public static void main(String[] args)
	{
		launch(args) ;
	}
	
	@Override
	public void start(Stage primaryStage)
	{
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/MainView.fxml"));
			root = (Parent) fxmlLoader.load();
			guiController = (InterfaceController) fxmlLoader.getController() ;
			
			app = App.getInstance() ;
			
			Scene scene = new Scene( root ) ;
			scene.getStylesheets().clear() ;
			
			primaryStage.setTitle( "Landing Gear System" );
			primaryStage.setScene ( scene ) ;
			primaryStage.show();
			primaryStage.setResizable(false);
			
			guiController.lights( app.lightPanel.getSystemState() );
			guiController.doorLights( app.controller.getDoorsCurrentState() );
			guiController.gearLights( app.controller.getGearsCurrentState() );
			
			app.startUpdateThread();
			app.startFeedbackThread();
			app.startDoorsGuiThread();
			app.startGearsGuiThread();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
