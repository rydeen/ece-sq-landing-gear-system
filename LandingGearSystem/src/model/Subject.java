package model;

/**
 * Subject class. This is an abstract class used to implement the Observer Design Pattern.
 * The Subject will notify the Observer to keep him informed about the current state.
 */
public abstract class Subject {

	
	private static int subjectindex = 0 ;
	private int mysubjectindex ;
	
	// id for the subject. the ID has two numbers
	// the first number represents one type of subject, and the seconds represents the id between classes
	private String id ;
	
	private Observer observer ;
	private int state ;
	
	
	/**
	 * Class constructor
	 */
	public Subject( int subjectCode , int subjectN ) {
		// each subject will have a different number to represent them
		// this number will be his index on the observer's ArrayList<Subject>
		this.id = "" + subjectCode + subjectN ;
		this.mysubjectindex = subjectindex++ ;
	}
	
	public Subject() {
		this.mysubjectindex = subjectindex++ ;
	}
	
	
	/**
	 * Sets the ID value of the subject
	 * @param code
	 * @param instance_n
	 */
	public void setSubjectID( int code , int instance_n ) {
		this.id = "" + code + instance_n ;
	}
	
	/**
	 * Retuns the ID value of the subject
	 * @return String id
	 */
	public String getId() {
		return this.id ;
	}
	
	/**
	 * Returns the id of the subject
	 * @return integer subjectindex
	 */
	public int getIndex() {
		return this.mysubjectindex ;
	}
	
	/**
	 * Returns the current state of the subject
	 * @return
	 */
	public int getState() {
		return this.state ;
	}
	
	/**
	 * Sets the current state of the subject to be 'state'
	 * @param state
	 */
	public synchronized void setState( int state ) {
		this.state = state ;
		
		// only send notification if there is an observer
		if (this.observer != null )
			notifyObserver() ;
	}
	
	/**
	 * Adds an Observer so it will be notified when changes occurs.
	 * The Observer will be notified with the current state too.
	 * @param observer
	 */
	public void attach( Observer observer ) {
		this.observer = observer ;
		notifyObserver();
	}
	
	/**
	 * Notification function. The observer will be notified to update his state.
	 */
	public void notifyObserver() {
			this.observer.update( mysubjectindex , id );
	}

}
