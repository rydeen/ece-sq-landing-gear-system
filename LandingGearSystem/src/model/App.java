package model;

import controller.Main;
import model.digitalpart.Controller;
import model.mechanicalparts.Door;
import model.mechanicalparts.Gear;
import model.mechanicalparts.GearSensor;
import model.ui.Handle;
import model.ui.HandleSensor;
import model.ui.LightPanel;
import system.exceptions.InvalidGearSensorType;
import system.exceptions.NegativeValueException;
import system.exceptions.NullStringException;

public class App {

	private static App instance = null ;
	
	
	public LightPanel lightPanel ;
	public HandleSensor handleSensor ;
	public Controller controller;
	public Handle handle ;
	public GearSensor gs1 ;		public Door door1 ;
	public GearSensor gs2 ;		public Door door2 ;
	public GearSensor gs3 ;		public Door door3 ;
	public GearSensor gs4 ;		public Gear gear1 ;
	public GearSensor gs5 ;		public Gear gear2 ;
	public GearSensor gs6 ;		public Gear gear3 ;
	
	private Thread threadGUI ;
	private Thread threadFeedback ;
	private Thread doorsGUI ;
	private Thread gearsGUI ;
	
	/**
	 * Use this class to get a new or already created instance of an 'App'.
	 * This is a Singleton class.
	 * @return
	 */
	public static App getInstance() {
		if ( instance == null )
			instance = new App() ;
		
		return instance ;
	}
	
	/**
	 * Class Constructor.
	 */
	private App() 
	{	
		lightPanel = LightPanel.getInstance() ; // light panel
		handleSensor = new HandleSensor() ; // handle sensor
		controller = Controller.getInstance() ; // controller
		controller.addSubject( handleSensor ); // adding handle subject to controller
		handle = Handle.getInstance() ; // creating handle and setting sensor
		handle.setSensor( handleSensor );
		
		gs1 = new GearSensor(GearSensor.Type.Door) ; // sensor for doors and doors creation
		gs2 = new GearSensor(GearSensor.Type.Door) ;
		gs3 = new GearSensor(GearSensor.Type.Door) ;
		door1 = null;
		door2 = null;
		door3 = null;
		
		try {
			door1 = new Door(2000,2000,"door1",gs1) ;
			door2 = new Door(2000,2000,"door2",gs2) ;
			door3 = new Door(2000,2000,"door3",gs3) ;
		} catch (NegativeValueException e) {
			e.printError();
		} catch (NullStringException e) {
			e.printError();
		}
		
		gs4 = new GearSensor(GearSensor.Type.Gear) ; // sensors for gears and gears creation
		gs5 = new GearSensor(GearSensor.Type.Gear) ;
		gs6 = new GearSensor(GearSensor.Type.Gear) ;
		gear1 = null;
		gear2 = null;
		gear3 = null;
		
		try {
			gear1 = new Gear(2000,2000,"gear1",gs4) ;
			gear2 = new Gear(2000,2000,"gear2",gs5) ;
			gear3 = new Gear(2000,2000,"gear3",gs6) ;
		} catch (NegativeValueException e) {
			e.printError();
		} catch (NullStringException e) {
			e.printError();
		} catch (InvalidGearSensorType e) {
			e.printError();
		}
		
		controller.setDoors( door1 , door2 , door3 ) ;	// controller adding doors and gears
		controller.setGears( gear1 , gear2 , gear3 ) ;
		
		controller.addSubject( gs1 );	// adding gearsensor subject to controller
		controller.addSubject( gs2 );
		controller.addSubject( gs3 );
		controller.addSubject( gs4 );
		controller.addSubject( gs5 );
		controller.addSubject( gs6 );

		controller.reportSystemState();
	}

	/**
	 * Creates and starts a thread to read the current system state received by the LightPanel class
	 */
	public void startUpdateThread() {
		this.threadGUI = new Thread() {
		    public void run() {
		    	while(true) {
					Main.guiController.lights( Main.app.lightPanel.getSystemState() );
				}
		    }  
		};
		this.threadGUI.start();
	}
	
	/**
	 * Creates and starts a thread to continuously reports the current system state to the LightPanel class.
	 */
	public void startFeedbackThread() {
		this.threadFeedback = new Thread() {
		    public void run() {
		    	while(true) {
					Main.app.controller.reportSystemState() ;
				}
		    }  
		};
		this.threadFeedback.start();
	}
	
	/**
	 * Creates and starts a thread to update the visual states of doors on GUI
	 */
	public void startDoorsGuiThread() {
		this.gearsGUI = new Thread() {
			public void run() {
				while(true) {
					Main.guiController.doorLights( Main.app.controller.getDoorsCurrentState() ) ;
				}
			}
		};
		this.gearsGUI.start();
	}
	
	/**
	 * Creates and starts a thread to update the visual state of gears on GUI
	 */
	public void startGearsGuiThread() {
		this.doorsGUI = new Thread() {
			public void run() {
				while(true) {
					Main.guiController.gearLights( Main.app.controller.getGearsCurrentState() ) ;
				}
			}
		};
		this.doorsGUI.start();
	}

}
