package model.ui;
import controller.Main;
import system.exceptions.InvalidSystemStateException;
import system.exceptions.UnknowColorException;

/**
 * LightPanel class. Implemented with the Singleton Design Pattern
 * This class represents the group of lights that will be available in the graphical user interface.
 * The Light class is nested inside this one.
 */
public final class LightPanel {

	private static LightPanel instance = null;
	
	/**
	 * Enum Color. These are the colors known by the Light class.
	 * used to keep LightPanel aware of the colors of his lights.
	 */
	enum Color { RED, GREEN, ORANGE } ;
	
	
	/* The 3 lights in the panel */
	private Light[] lights ;
	private int systemState ;
	
	/**
	 * Class constructor.
	 */
	private LightPanel()
	{
		this.lights = new Light[3] ;
		
		// the Light will throw an exception if the Color is unknown for him.
		try {
			this.lights[0] = new Light( "R" , Color.RED ) ;
			this.lights[1] = new Light( "O" , Color.ORANGE ) ;
			this.lights[2] = new Light( "G" , Color.GREEN ) ;
		} catch (UnknowColorException e) {
			e.printError();
		}

		//System.out.println("[LP] \t LightPanel created");
	}
	
	/**
	 * This method should be used instead of a constructor.
	 * LightPanel is a singleton class.
	 * @return LightPanel
	 */
	public static LightPanel getInstance() {
		if ( instance == null ) {
			instance = new LightPanel() ;
		}
		else {
			//System.out.println("[LP] \t LightPanel instance returned");
		}
		
		return instance;
	}
	
	
	/**
	 * Turns on the light with the id "lightID" and turns off all the other lights
	 * @param lightID
	 */
	public void turnOn( String lightID )
	{
		for( Light l : this.lights )
		{
			if ( l.getID().equals( lightID) )
				l.turnOn();
			else
				l.turnOff();
		}
	}
	/**
	 * Turns off all the lights in the panel
	 */
	public void turnOff()
	{
		for ( Light l : this.lights )
			l.turnOff();
	}

	/**
	 * Receives the current state of the system and lights up a specific light
	 * @param value
	 */
	public synchronized void systemStateReceiver( int value ) throws InvalidSystemStateException
	{
		//System.out.println("[LP] \t System State Received with value=" + value );
		
		if ( value == 0 ) // means gears retracted and doors closed
			this.turnOff();
		else if ( value == 1 ) // means gears extended and doors closed
			this.turnOn("G");
		else if ( value == 2 ) // means gears and/or doors maneuvering
			this.turnOn("O") ;
		else if ( value == 3 ) // means mechanical failure
			this.turnOn("R");
		else
			throw new InvalidSystemStateException(value) ;
		
		this.systemState = value ;
		
		if ( Main.guiController != null )
			Main.guiController.lights( this.systemState );
	}
	
	public synchronized int getSystemState() {
		return this.systemState ;
	}
	
	
	/*------------------------------------------------------------------------------*/
	//								  LIGHT CLASS									//
	/*------------------------------------------------------------------------------*/
	
	/**
	 * LIGHT
	 * This class represents a light in the graphical user interface.
	 * The color can be red, green or orange. 
	 * { color="#FF0000" or color="#008000" or color="#FFA500" }
	 */
	private class Light {
		private String id ;
		private String color ;
		private boolean lightOn ;
		
		/**
		 * Class constructor
		 * @param id
		 * @param color
		 * @throws UnknowColorException 
		 */
		public Light( String id , Color color ) throws UnknowColorException
		{
			this.id = id ;
			this.color = getColorString(color) ;
			this.lightOn = false ;
			
			//System.out.println("[L] \t Light created with id=" + this.id );
		}
		
		/**
		 * Turns on the light.
		 */
		public void turnOn() { 
			//System.out.println("[L] \t Light " + this.id + " turned ON");
			this.lightOn =true ; 
		}
		/**
		 * Turns off the light.
		 */
		public void turnOff() {
			//System.out.println("[L] \t Light " + this.id + " turned OFF");
			this.lightOn = false ; 
		}
		
		/**
		 * Returns the a string representing the color hex code.
		 * @return String color hex code
		 */
		@SuppressWarnings("unused")
		public String getColor() { return this.color ; }
		
		/**
		 * Returns the current state of the light, which can be ON or OFF.
		 * @return Boolean current state. TRUE if state=ON, FALSE otherwise.
		 */
		@SuppressWarnings("unused")
		public boolean getState() { return this.lightOn ; }
		
		/**
		 * Returns the id of the Light object.
		 * @return String id
		 */
		public String getID() { return this.id ; }
		
		/**
		 * Given a Color, returns the hex code of that color.
		 * @param color
		 * @return String hex color code
		 */
		private String getColorString( Color color ) throws UnknowColorException {
			switch( color )
			{
				case RED: 
					return "#FF0000" ;
				case GREEN: 
					return "#008000" ;
				case ORANGE: 
					return "#FFA500" ;
				default:  
					throw new UnknowColorException( color.toString() );
			}
		}
	}
	
}
