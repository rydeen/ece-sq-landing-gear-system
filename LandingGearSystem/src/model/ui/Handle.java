package model.ui;
import java.util.Date;

import model.digitalpart.Controller;

/**
 * Handle class. Represents the Handle of the 'Landing Gear System'
 * Implemented as a singleton class.
 */
public final class Handle {

	private static Handle instance = null;
	
	private Controller ctrl_ref ;
	
	private int position ; // if 1 means Handle UP; 0 means DOWN
	private long timestamp ; // time of the last change
	private long minTime ; // time between changes of state
		
	private boolean analogicalSwitchClosed ;
	public HandleSensor sensor ;
	
	Thread AnalogicalSwitchClock ;
	
	/**
	 * Class constructor.
	 */
	private Handle()
	{
		this.position = 0 ;
		this.timestamp = 0l ;
		this.minTime = 5000l ;
		
		this.analogicalSwitchClosed = false ;
				
		this.ctrl_ref = Controller.getInstance() ;
		
		//System.out.println("[H] \t Handle created");
	}
	
	/**
	 * This should be used instead of the class constructor.
	 * The Handle is a singleton class.
	 * @return Handle. A new object if it's the first time, or the same already created before.
	 */
	public static Handle getInstance() {
		if ( instance == null ) {
			instance = new Handle() ;
		}
		else {
			//System.out.println("[H] \t Handle instance returned");
		}
		
		return instance;
	}
	
	/**
	 * Set the value of the sensor
	 * @param hs
	 */
	public void setSensor( HandleSensor hs ) {
		this.sensor = hs ;
	}
	
	/**
	 * Changes the state of the Handle.
	 * The state will be changed in this class. The HandleSensor will be ordered to change his state to.
	 * Also, the analogical switch will be closed.
	 * There is a minimum time state changes. It will fail when trying to change state within this time.
	 * The minimum value can be retrieved with the function 'getMinTime()'
	 * @return Boolean. TRUE if the state was changed with success or FALSE otherwise.
	 */
	public boolean changeState()
	{
		if ( (new Date().getTime() - this.timestamp) > this.minTime )
		{
			if ( this.position == 1 )
				this.position = 0 ;
			else
				this.position = 1 ;
			
			// closing analogical switch
			this.analogicalSwitchClosed = true ;
			// sending information to the Controller
			this.ctrl_ref.analogicalSwitchState( this.analogicalSwitchClosed );
						
			// changing HandleSensor state
			this.sensor.changeOfState() ;
			
			// registering current timestamp for the current state change
			this.timestamp = new Date().getTime() ;
			
			// creating and starting a thread to count the time before opening the analogical switch again
			this.AnalogicalSwitchClock = new Thread( new AnalogicalSwitchClockThread(this) ) ;
			this.AnalogicalSwitchClock.start();
			
			//System.out.println("[H] \t Handle State changed to " + (this.position==1? "UP" : "DOWN"));
			return true ;
		}
		else
		{
			//System.out.println("[H] \t Handle State NOT changed");
			return false ;
		}
	}
	
	/**
	 * Returns the minimum value of time between two state changes.
	 * @return long minTime
	 */
	public long getMinTime() { return this.minTime ; }
	
	/**
	 * Returns the timestamp of the last state change.
	 * @return long timestamp
	 */
	public long getTimeStamp() { return this.timestamp ; }
	
	/**
	 * Turns off the analogical switch and notifies the controller it is now open again.
	 */
	public synchronized void turnOffAnalogicalSwitch() { 
		this.analogicalSwitchClosed = false ;
		this.ctrl_ref.analogicalSwitchState(this.analogicalSwitchClosed);
	}

	/*------------------------------------------------------------------------------*/
	//					 ANALOGICAL SWITCH CLOCK THREAD								//
	/*------------------------------------------------------------------------------*/
	/**
	 * Thread to count the time left until opening the analogical switch again.
	 */
	private class AnalogicalSwitchClockThread implements Runnable {

		private Handle handle_ref ;
		
		public AnalogicalSwitchClockThread( Handle handle ) {
			this.handle_ref = handle ;
			//System.out.println("[AS_T] \t Analogical Switch Clock Thread created");
		}
		
		@Override
		public void run() {
			//System.out.println("[AS_T] \t Thread started...");
			
			while( (new Date().getTime() - handle_ref.getTimeStamp())<=handle_ref.getMinTime() ) {
			}
			
			// after the Handle.minTime, the analogical switch is opened again.
			handle_ref.turnOffAnalogicalSwitch() ;
			
			//System.out.println("[AS_T] \t Thread done.");
		}
		
	}
	
	
}
