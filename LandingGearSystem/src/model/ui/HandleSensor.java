package model.ui;
import model.Subject;

/**
 * HandleSensor class. Represents the 'sensor' that keeps the value of the current state of the Handle.
 * This class is also a 'Subject' that can be subscribed to be notified about state changes.
 */
public final class HandleSensor extends Subject {

	private static int instanceN = 0 ;
	private final static int idCode = 1 ;
	
	
	/* state=1 -> Handle UP
	 * state=0 -> Handle DOWN 
	 */
	
	
	/**
	 * Class constructor.
	 */
	public HandleSensor()
	{
		super( idCode , instanceN );
		instanceN++;
		this.setState(0);
		
		//System.out.println("[HS] \t HandleSensor created. Subject " + this.getIndex() + ". Subject id=" + this.getId());
	}
	
	/**
	 * Changes the current state. 
	 * If the current state is 1 (meaning Handle UP), it will be changed to 0 (menaing Handle DOWN).
	 */
	public void changeOfState() {
		if ( this.getState() == 1 )
			this.setState(0);
		else
			this.setState(1);

		//System.out.println("[HS] \t HandleSensor State changed to " + this.getState() );
	}
	
	/**
	 * Returns the current state value.
	 * @return Int state
	 */
	public int getSensorValue() {
		return this.getState() ;
	}
	
}
