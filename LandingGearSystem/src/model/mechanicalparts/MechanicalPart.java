package model.mechanicalparts;

import system.exceptions.InvalidSystemStateException;
import system.exceptions.NegativeValueException;
import system.exceptions.NullStringException;

/**
 * MechanicalPart class. 
 * Door and Gear are child classes of this one.
 */
public abstract class MechanicalPart {

		/*
		 * States available:
		 * 		1. Open - Extend
		 * 		2. Close - Retract
		 * 		3. Maneuvering
		 */
		
		// sensor for this object
		private GearSensor sensor ;

		// times needed to open and close
		protected float openTime ;
		protected float closeTime ;
		
		// object id
		protected String nameId ;
		
		/**
		 * Class constructor.
		 * @param openTime
		 * @param closeTime
		 * @param id
		 * @param gs
		 * @throws NullStringException if id is not valid
		 * @throws NegativeValueException if closeTime or openTime are negative
		 */
		public MechanicalPart(float openTime, float closeTime, String id, GearSensor gs )
				throws NullStringException, NegativeValueException
		{
			if ( id == null )
			{
				//System.out.println("[MP] \t Invalid id detected.");
				throw new NullStringException( "GearID" ) ;
			}
			if ( openTime <= 0.0 )
			{
				//System.out.println("[MP] \t openTime must be greater than 0.");
				throw new NegativeValueException( "openTime" , openTime ) ;
			}
			if ( closeTime <=0.0 )
			{
				//System.out.println("[MP] \t closeTime must be greater than 0.");
				throw new NegativeValueException( "closeTime" , closeTime ) ;
			}
			
			this.openTime = openTime ;
			this.closeTime = closeTime ;
			this.nameId = id ;
			
			this.sensor = gs ;
		}

		/**
		 * Change current state to 'newstate'.
		 * Updates the 'state' variable and the 'state' of the sensor.
		 * @return TRUE if the change was made, or FALSE otherwise
		 */
		public synchronized boolean changeState( int newstate ) 
		{	
			try {
				this.sensor.changeState( newstate ) ;
			} catch (InvalidSystemStateException e) {
				e.printError();
				//System.out.println("[MP] \t " + this.nameId + " could not change his state.");
				return false ;
			}
			
			//System.out.println("[MP] \t " + this.nameId + " changed is state to " + this.state );
			return true ;	
		}
}
