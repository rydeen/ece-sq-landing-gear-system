package model.mechanicalparts;

import java.util.Date;

import system.exceptions.InvalidActionOrder;
import system.exceptions.InvalidGearSensorType;
import system.exceptions.NegativeValueException;
import system.exceptions.NullStringException;

/**
 * Gear class. Represents a gear of the aircraft.
 */
public class Gear extends MechanicalPart {

	/**
	 * Constructor class
	 * @param openTime
	 * @param closeTime
	 * @param id
	 * @param gs
	 * @throws NullStringException
	 * @throws NegativeValueException
	 * @throws InvalidGearSensorType 
	 */
	public Gear(float openTime, float closeTime, String id, GearSensor gs)
			throws NullStringException, NegativeValueException, InvalidGearSensorType 
	{	
		super(openTime, closeTime, id, gs);
		
		if ( gs.sensorType != GearSensor.Type.Gear )
			throw new InvalidGearSensorType(gs.sensorType.toString() , GearSensor.Type.Gear.toString(), id) ;
		
		//System.out.println("[G] \t Door created with id=" + this.nameId);	
	}

	
	/**
	 * Function to order the Gear to extend.
	 */
	public void extend()
	{
		//System.out.println("[D] \t" + this.nameId +" Extending gear...");
		try {
			new Thread(new GearActionPerformer(this,1)).start() ;
		} catch (InvalidActionOrder e) {
			e.printError();
		}
	}
	
	/**
	 * Function to order the Gear to retract.
	 */
	public void retract()
	{		
		//System.out.println("[D] \t" + this.nameId +" Retracting gear...");
		try {
			new Thread(new GearActionPerformer(this,0)).start() ;
		} catch (InvalidActionOrder e) {
			e.printError();
		}
	}
	
	/*------------------------------------------------------------------------------*/
	//							 ACTION 	PERFORMER								//
	/*------------------------------------------------------------------------------*/
	/**
	 * Thread to perform the orders received by the controller.
	 */
	private class GearActionPerformer implements Runnable {
		private Gear ref ;
		private int action ;
		
		public GearActionPerformer( Gear gref, int action ) throws InvalidActionOrder {
			this.ref = gref ;
			if ( action != 0 && action != 1 )
				throw new InvalidActionOrder() ;
			else
				this.action = action ;
		}
		
		@Override
		public void run() 
		{
			long timestamp = new Date().getTime() ;
			
			if ( action == 1 ) // extend
			{
				while( (new Date().getTime() - timestamp) <= this.ref.openTime ) {
					this.ref.changeState(3) ;
				}
				this.ref.changeState(1) ;
				//System.out.println("[GAP_T] \t Gear is now opened. " + ref.nameId);
			}
			else // retract
			{
				while( (new Date().getTime() - timestamp) <= this.ref.closeTime ) {
					this.ref.changeState(3) ;
				}
				this.ref.changeState(2) ;
				//System.out.println("[GAP_T] \t Gear is now opened. " + ref.nameId);
			}
		}
	}
	
}