package model.mechanicalparts;

import java.util.Date;

import system.exceptions.InvalidActionOrder;
import system.exceptions.NegativeValueException;
import system.exceptions.NullStringException;

/**
 * Door class. Represents a door for the gears in the aircraft.
 */
public class Door extends MechanicalPart {
	
	/**
	 * Constructor class
	 * @param openTime
	 * @param closeTime
	 * @param id
	 * @param gs
	 * @throws NullStringException
	 * @throws NegativeValueException
	 */
	public Door(float openTime, float closeTime, String id, GearSensor gs)
			throws NullStringException, NegativeValueException 
	{	
		super(openTime, closeTime, id, gs);
		//System.out.println("[D] \t Door created with id=" + this.nameId);	
	}

	
	/**
	 * Function to order the Door to open.
	 */
	public void open()
	{
		//System.out.println("[D] \t Opening door " + this.nameId);
		try {
			new Thread(new DoorActionPerformer(this,1)).start() ;
		} catch (InvalidActionOrder e) {
			e.printError();
		}
		// 2. set sensor state to maneuvering
		// 3. count time to finish the action
		// 4. set sensor state to open	
	}
	
	/**
	 * Function to order the Door to close.
	 */
	public void close()
	{
		//System.out.println("[D] \t Closing doors " + this.nameId);
		try {
			new Thread(new DoorActionPerformer(this,0)).start() ;
		} catch (InvalidActionOrder e) {
			e.printError();
		}
		// 2. set sensor state to maneuvering
		// 3. count time to finish the action
		// 4. set sensor state to close
	}
	
	/*------------------------------------------------------------------------------*/
	//							 ACTION 	PERFORMER								//
	/*------------------------------------------------------------------------------*/
	/**
	 * Thread to perform the orders received by the controller.
	 */
	private class DoorActionPerformer implements Runnable {
		private Door ref ;
		private int action ;
		
		public DoorActionPerformer( Door dref, int action ) throws InvalidActionOrder {
			this.ref = dref ;
			if ( action != 0 && action != 1 )
				throw new InvalidActionOrder() ;
			else
				this.action = action ;
		}
		
		@Override
		public void run() 
		{
			long timestamp = new Date().getTime() ;
			
			if ( action == 1 ) // open
			{
				while( (new Date().getTime() - timestamp) <= this.ref.openTime ) {
					this.ref.changeState(3) ;
				}
				this.ref.changeState(1) ;
				//System.out.println("[DAP_T] \t Door is now opened. " + ref.nameId);
			}
			else // close
			{
				while( (new Date().getTime() - timestamp) <= this.ref.closeTime ) {
					this.ref.changeState(3) ;
				}
				this.ref.changeState(2) ;
				//System.out.println("[DAP_T] \t Door is now closed. " + ref.nameId);
			}
		}
	}
	
}