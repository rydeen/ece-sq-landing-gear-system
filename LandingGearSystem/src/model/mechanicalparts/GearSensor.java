package model.mechanicalparts;
import model.Subject;
import system.exceptions.InvalidSystemStateException;

/**
 * GearSensor class. Represents the sensors for gears and doors of the aircraft.
 * Keeps the current state of each one.
 * This class is also a 'Subject' that can be subscribed to be notified about state changes.
 */
public class GearSensor extends Subject {

	private static int instanceGear = 0 ;
	private static int instanceDoor = 0 ;
	private final static int idCodeGear = 2 ;
	private final static int idCodeDoor = 3 ;
	
	// to distinguish between sensor for doors and sensor for gears
	public static enum Type { Gear , Door }
	
	// type of mechanical part associated with this sensor
	public Type sensorType ;
	
	/*
	 * States available:
	 * 		1. Open - Extend
	 * 		2. Close - Retract
	 * 		3. Maneuvering
	 */
	
	/**
	 * Class constructor.
	 */
	public GearSensor( Type t )
	{
		super() ;
		switch( t )
		{
			case Gear:
				this.setSubjectID( idCodeGear , instanceGear ) ;
				instanceGear++;
				this.setState(1);
				break;
			case Door:
				this.setSubjectID( idCodeDoor, instanceDoor) ;
				instanceDoor++;
				this.setState(2);
				break;
		}
		
		this.sensorType = t ;
		
		//System.out.println("[GDS] \t GearSensor for " + t + ". Subject " + this.getIndex() + ". Subject id=" + this.getId() );
	}
	
	/**
	 * Changes the current state of the sensor to the value 'value'
	 * @param value
	 * @return TRUE if the change was made with success or FALSE otherwise
	 */
	public synchronized boolean changeState( int value ) throws InvalidSystemStateException {
		
		// check if the 'value' is an acceptable state.
		if ( !(value>=1 && value<=3) )
		{
			//System.out.println("[GDS] \t Aborted. Invalid Sensor State -> " + value );
			throw new InvalidSystemStateException( value ) ;
		}
		
		this.setState( value );
		
		//System.out.println("[GDS] \t Sensor state changed to " + this.getState() );
		return true ;
	}
	
}
