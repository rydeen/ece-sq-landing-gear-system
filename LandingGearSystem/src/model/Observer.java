package model;

import java.util.ArrayList;
import java.util.List;

/**
 * Observer class. This is an abstract class used to implement the Observer Design Pattern.
 * This class represents the objects that have subscribed to a group of Subjects.
 */
public abstract class Observer {
	
	// List of subjects subscribed
	public List<Subject> subjects = new ArrayList<Subject>() ;
	
	/**
	 * Subscribe to a subject to receive notifications from him.
	 * @param s
	 */
	public void subscribe( Subject s ) {
		this.subjects.add( s ) ;
	}
	
	/**
	 * Update the subject 'subjectid' that is stored in the 'subjectindex' position of the List<Subject>
	 * @param subjectindex
	 * @param subjectid
	 */
	public abstract void update( int subjectindex , String subjectid ) ;

}
