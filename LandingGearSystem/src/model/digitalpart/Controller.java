package model.digitalpart;


import model.Observer;
import model.Subject;
import model.mechanicalparts.Door;
import model.mechanicalparts.Gear;
import model.ui.LightPanel;
import system.exceptions.InvalidActionOrder;
import system.exceptions.InvalidSystemStateException;

/**
 * Controller class. Represents the 'Digital Part' of the Landing Gear System.
 * This class is responsible for keeping the LightPanel up to date to inform the pilot about the current system state.
 * Also, this class will be an Observer of all the sensors (GearSensor and HandleSensor) and will be the one that
 * will ordering Doors and Gears to take some action.
 * 
 * Singleton Design Pattern implemented in this class.
 */
public class Controller extends Observer {
	
	private static Controller instance = null;
	
	// LighPanel reference
	private LightPanel lp_ref ;
	// Gears reference
	private Gear[] gears_ref ;
	// Doors reference
	private Door[] doors_ref ;
	
	// Enumeration with all the possible system states.
	public enum SystemState { RETRACTED, DOWN, MANEUVERING, FAILURE } ;
	
	// current handle state
	private int handleSensorCurrentState ;
	
	// current gears sensors state
	private int[] gearSensorsCurrentStates ;
	// current doors sensors state
	private int[] doorSensorsCurrentStates ;
	
	
	// current system state (value to be reported to LightPanel)
	public SystemState state = SystemState.RETRACTED;
	
	// current state of the Analogical Switch. TRUE if it's closed. FALSE if it's open.
	private boolean ge_active = false ;
	
	

	/**
	 * Class constructor
	 */
	private Controller() {
		this.lp_ref = LightPanel.getInstance() ;
		
		// there are 1 sensor for each door and for each gear.
		this.gearSensorsCurrentStates = new int[3] ;
		this.doorSensorsCurrentStates = new int[3] ;
		
		// gears and doors references
		this.gears_ref = new Gear[3] ;
		this.doors_ref = new Door[3] ;
		
		//System.out.println("[CTRL] \t Controller created");
	}
	
	/**
	 * This is a singleton class. It can be created only once.
	 * @return an instance of the class. If it's the first time it will return a new one, else it will return the previous one created.
	 */
	public static Controller getInstance() {
		if ( instance == null ) {
			instance = new Controller() ;
		}
		else {
			// System.out.println("[CTRL] \t Controller instance returned");
		}
		
		return instance;
	}
	
	/* ------------ GEARS ------------ */
	/**
	 * Define the 3 gears of the aircraft to control
	 * @param g1
	 * @param g2
	 * @param g3
	 */
	public void setGears( Gear g1, Gear g2, Gear g3 ) {
		this.gears_ref[0] = g1 ;
		this.gears_ref[1] = g2 ;
		this.gears_ref[2] = g3 ;
	}
	
	/**
	 * Returns an array with the state of each gear of the aircraft
	 * @return integer []
	 */
	public int[] getGearsCurrentState() {
		return this.gearSensorsCurrentStates ;
	}
	
	/* ------------ DOORS ------------ */
	/**
	 * Define the 3 doors of the gears' aircraft to control
	 * @param d1
	 * @param d2
	 * @param d3
	 */
	public void setDoors( Door d1, Door d2, Door d3 ) {
		this.doors_ref[0] = d1 ;
		this.doors_ref[1] = d2 ;
		this.doors_ref[2] = d3 ;
	}
	
	/**
	 * Returns an array with the state of each door of the aircraft
	 * @return
	 */
	public int[] getDoorsCurrentState() {
		return this.doorSensorsCurrentStates ;
	}
	
	/* ------------ LIGHTS ------------ */
	
	/**
	 * Report system state to the LightPanel
	 * @return TRUE if the system state was translated with success by the LightPanel, or FALSE otherwise.
	 */
	public synchronized boolean reportSystemState()
	{
		// all doors closed
		if ( doorSensorsCurrentStates[0]==2 && doorSensorsCurrentStates[1]==2 && doorSensorsCurrentStates[2]==2 )
		{
			// all gears retracted
			if ( gearSensorsCurrentStates[0]==2 && gearSensorsCurrentStates[1]==2 && gearSensorsCurrentStates[2]==2 )
			{
				this.state = SystemState.RETRACTED ;
			}
			// all gears extended
			else if ( gearSensorsCurrentStates[0]==1 && gearSensorsCurrentStates[1]==1 && gearSensorsCurrentStates[2]==1 )
			{
				this.state = SystemState.DOWN ;
			}
			else // gears failure
			{
				this.state = SystemState.FAILURE ;
			}
		}
		else
		{
			this.state = SystemState.MANEUVERING ;
		}

		//System.out.println("\n[CTRL] \t Reporting System State with value=" + this.state );
		
		switch ( this.state )
		{
			case RETRACTED:
				try {
					this.lp_ref.systemStateReceiver(0);
				} catch (InvalidSystemStateException e) {
					e.printError();
					return false ;
				}
				break;
			case DOWN:
				try {
					this.lp_ref.systemStateReceiver(1);
				} catch (InvalidSystemStateException e) {
					e.printError();
					return false ;
				}
				break;
			case MANEUVERING:
				try {
					this.lp_ref.systemStateReceiver(2);
				} catch (InvalidSystemStateException e) {
					e.printError();
					return false ;
				}
				break;
			case FAILURE:
				try {
					this.lp_ref.systemStateReceiver(3);
				} catch (InvalidSystemStateException e) {
					e.printError();
					return false ;
				}
				break;
		}
		
		return true ;
	}
	
	/* ------------ HANDLE ------------ */
	
	/**
	 * Receives the value of the analogical circuit. False means OPEN, and True means CLOSED
	 * @param ass
	 */
	public void analogicalSwitchState( boolean ass )  {
		//System.out.println("[CTRL] \t Analogical Switch State received with value=" + ass );
		this.ge_active = ass ;
	}
	
	public void handleAction() 
	{	
		if ( this.ge_active )
		{
			if ( this.handleSensorCurrentState == 1 ) // handle is UP
			{
				try {
					new Thread( new ControllerAction(this,1) ).start();
					//System.out.println("[CTRL] \t Retracting sequence order sent.");
				} catch (InvalidActionOrder e) {
					e.printError();
				}
			}
			else if ( this.handleSensorCurrentState == 0 ) // handle is DOWN
			{
				try {
					new Thread( new ControllerAction(this,0) ).start();
					//System.out.println("[CTRL] \t Extending sequence order sent.");
				} catch (InvalidActionOrder e) {
					e.printError();
				}
			}
		}
		else {
			//System.out.println("[CTRL] Handle action not performed. Analogical Switch is open.");
		}
	}

	/* ------------ SUBJECTS ------------ */
	/**
	 * Adds the Subject to be subscribed.
	 * @param s
	 */
	public void addSubject( Subject s ) {
		this.subscribe( s ) ;
		//System.out.println("[CTRL] \t Controller subscribed to subject " + s.getIndex() );
		this.subjects.get( s.getIndex()  ).attach( this );
	}
	
	@Override
	/**
	 * This method will be used when a Subject sends a notification to the observer.
	 * The argument will indicate which subject has sent the message and which state should be updated
	 * @param subjectindex
	 */
	public synchronized void update( int subjectindex , String subjectid ) {
		//System.out.println("[CTRL] \t Time to update subject " + subjectindex + " with id=" + subjectid );
		// ID -> XY
		// X { 1 , 2 , 3 } --- code for the sensor type. 1 for handle. 2 for gears. 3 for doors.
		// Y { 1 , ... n } --- number of the instantiation
		String auxtype = subjectid.substring(0,1) ;
		int auxindex = Integer.parseInt(subjectid.substring(1)) ;
		switch( auxtype )
		{
			case "1":
				this.handleSensorCurrentState = this.subjects.get( subjectindex ).getState() ;
				//System.out.println("[CTRL] \t Update: handle is now " + (this.handleSensorCurrentState==1? "UP" : "DOWN") );
				this.handleAction();
				break;
			case "2":
				this.gearSensorsCurrentStates[auxindex] = this.subjects.get( subjectindex ).getState() ;
				//System.out.println("[CTRL] \t Update: GearSensor " + subjectid + " state changed to " + this.gearSensorsCurrentStates[auxindex]);
				break;
			case "3": 
				this.doorSensorsCurrentStates[auxindex] = this.subjects.get( subjectindex ).getState() ;
				//System.out.println("[CTRL] \t Update: GearSensor " + subjectid + " state changed to " + this.doorSensorsCurrentStates[auxindex]);
				break;
		}
	}
	
	
	
	
	/*------------------------------------------------------------------------------*/
	//							 ACTION ORDER SENDER								//
	/*------------------------------------------------------------------------------*/
	/**
	 * Thread to send the orders to the doors and gears.
	 */
	private class ControllerAction implements Runnable {

		int action ;
		Controller ref ;
		
		public ControllerAction( Controller c , int action ) throws InvalidActionOrder {
			if ( action == 1 || action == 0)
				this.action = action ;
			else
				throw new InvalidActionOrder() ;
			
			this.ref = c ;
		}
		
		@Override
		public void run() 
		{
			if ( action == 1 ) // retracting sequence
			{
				// order doors to open
				for ( Door d : ref.doors_ref )
					d.open();
				
				// wait until all doors are open
				while( ! doorsOpen() ) {
					System.out.print("");
				}
				
				System.out.print("");
				
				// order gears to retract
				for ( Gear g : ref.gears_ref )
					g.retract();
				
				// wait until all gears are retracted
				while ( ! gearsRetracted() ) {
					System.out.print("");
				}
				
				System.out.print("");
				
				// order doors to close
				for ( Door d : ref.doors_ref )
					d.close();
				
				// wait until all doors are not closed
				while ( doorsOpen() ) {
					System.out.print("");
				}
				
				System.out.print("");
			}
			else // extending sequence 
			{
				// order doors to open
				for ( Door d : ref.doors_ref )
					d.open();
				
				// wait until all doors are open
				while( ! doorsOpen() ) {
					System.out.print("");
				}
				
				System.out.print("");
				
				// order gears to extend
				for ( Gear g : ref.gears_ref )
					g.extend();
				
				// wait until all gears are extended
				while ( ! gearsExtended() ) {
					System.out.print("");
				}
				
				System.out.print("");
				
				// order doors to close
				for ( Door d : ref.doors_ref )
					d.close();
				
				// wait until all doors are not closed
				while ( doorsOpen() ) {
					System.out.print("");
				}
				
				System.out.print("");
			}
		}
		
		public boolean doorsOpen() {
			if ( ref.doorSensorsCurrentStates[0]==1 && ref.doorSensorsCurrentStates[1]==1 && ref.doorSensorsCurrentStates[2]==1 )
				return true ;
			else return false ;
		}
		public boolean gearsExtended() {
			if ( ref.gearSensorsCurrentStates[0]==1 && ref.gearSensorsCurrentStates[1]==1 && ref.gearSensorsCurrentStates[2]==1 )
				return true ;
			else return false ;
		}
		public boolean gearsRetracted() {
			if ( ref.gearSensorsCurrentStates[0]==2 && ref.gearSensorsCurrentStates[1]==2 && ref.gearSensorsCurrentStates[2]==2 )
				return true ;
			else return false ;
		}
	}
	
	
	
}
